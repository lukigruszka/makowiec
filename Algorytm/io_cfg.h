#ifndef _io_cfg_h_
#define _io_cfg_h_

#define F_CPU 			8000000L

#define StartModuleInit		DDRD	&= ~(1<<PD3 | 1<< PD4)
#define KillSwitch			PIND	& (1<<PD3)
#define Start				PIND	& (1<<PD4)

#define LED_INIT			DDRB	|= (1<<PB7)
#define LED_ON				PORTB	|= (1<<PB7)
#define LED_OFF				PORTB 	&= ~(1<<PB7)

#define IR_INIT				DDRB 	&= ~(1<<PB6);  DDRC &= ~(1<<PC2 | 1<<PC3)//; PORTC &= ~(1<<PC2 | 1<<PC3); PORTB &= ~(1<<PB6);
#define IR_Right			PINC 	& (1<<PC2)
#define IR_Middle			PINC	& (1<<PC3)	
#define IR_Left				PINB 	& (1<<PB6)

#define Line_INIT			DDRC 	&= (1<<PC1 | 1<<PC0); PORTC |= (1<<PC1 | 1<<PC0)
#define LineLeft			PINC	& (1<<PC1)
#define LineRight			PINC	& (1<<PC0)

#define H_INIT				DDRD	|= (1<<PD5 | 1<<PD6 | 1<<PD7); DDRB |= (1<<PB0 | 1<<PB1 | 1<<PB2)
#define MotorR_PWM 			OCR0B 
#define MotorR_Ahead		PORTD	|= (1<<PD6); PORTD &= ~(1<<PD7)
#define MotorR_Back			PORTD	|= (1<<PD7); PORTD &= ~(1<<PD6)
#define MotorR_SoftStop		PORTD	|= (1<<PD6 | 1<<PD7)
#define MotorR_FullStop		PORTD	&= ~(1<<PD6 | 1<<PD7)
#define MotorL_PWM 			OCR1B
#define MotorL_Ahead		PORTB	|= (1<<PB0); PORTD &= ~(1<<PB1)
#define MotorL_Back			PORTB	|= (1<<PB1); PORTD &= ~(1<<PB0)
#define MotorL_SoftStop		PORTB	|= (1<<PB0 | 1<<PB1)
#define MotorL_FullStop		PORTB	&= ~(1<<PB0 | 1<<PB1)


#endif _io_cfg_h_
