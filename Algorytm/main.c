#include <util/delay.h>
#include <avr/io.h>
//#include <uart.h>
#include "io_cfg.h"

void init(void);
void FirstMove(void);
void PWM(int left, int right);
void SoftStop();
void FullStop();

int main(){
	int i;
	//_delay_ms(1000000);
	_delay_ms(1001);
	init();
	
	/*while(!Start){
		_delay_ms(1);
	}*/
	
	FirstMove();
	
	while(1){ //KillSwitch
		if(IR_Middle)
			PWM(255, -255);
		//else if((IR_Right))
		//	PWM(255, 150);
		else
			PWM(255, 255);
	}
	
}

void FirstMove(){
	PWM(255, -255);
	_delay_ms(250);
}

void PWM(int left, int right) { 
    if(left >= 0){ 
        MotorL_Ahead;
    }
    else {
		MotorL_Back;
	}
    
    if(right >= 0){
		MotorR_Ahead;
	}
    else{
		MotorR_Back;
	}
    
    MotorL_PWM = abs(left); //abs(x) --> zwraza wart. bezwzględną liczby x
    MotorR_PWM = abs(right); 
}

void SoftStop(){
	MotorL_SoftStop;
	MotorR_SoftStop;
}

void FullStop(){
	MotorL_FullStop;
	MotorR_FullStop;
}
	
void init(){
	LED_INIT;
	H_INIT;
	IR_INIT;
	Line_INIT;
	StartModuleInit;
	pwm_setup();
}

void pwm_setup() {
	/* Timer0 non-inverting mode */
	//TCCR0A |= ((1<<COM0A1) | (1<<COM0B1)); 
	//TCCR0A &= ~((1<<COM0A0) | (1<<COM0B0)); 
	TCCR0A |= (1<<COM0B1);
	TCCR0A &= ~(1<<COM0B0); 
	
	/* Timer0 Fast PWM, 8-bit */
	TCCR0A |= ((1<<WGM00) | (1<<WGM01));
	TCCR0B &= ~(1<<WGM02);
	//TCCR0B |= (1<<WGM02);
	
	/* Timer0 no-prescaling - 8MHz / (2^8) = 31.2kHz */
	TCCR0B &= ~((1<<CS02) | (1<<CS01));
	TCCR0B |= (1<<CS00); //gggggggggggggggg

	/* Timer1 non-inverting mode */
	TCCR1A |= ((1<<COM1A1) | (1<<COM1B1)); 
	TCCR1A &= ~((1<<COM1A0) | (1<<COM1B0)); 
	
	/* Timer1 Fast PWM, 8-bit */
	TCCR1A |= (1<<WGM10);
	TCCR1A &= ~(1<<WGM11);
	TCCR1B |= (1<<WGM12);
	TCCR1B &= ~(1<<WGM13);
	
	/* Timer1 no-prescaling - 8MHz / 2^8 = 31.2kHz */
	TCCR1B |= (1<<CS10);
	TCCR1B &= ~((1<<CS11) | (1<<CS12));
	
	/* Przykładowe wartości (0-1023) */
	//MotorL_PWM = 1023;
	//MotorR_PWM = 1023;
	
	/*OCR1AH = 0xFF;
	OCR1AL = 0xFF;
	
	OCR1BH = 0xFF;
	OCR1BL = 0xFF;*/
}
