# Makowiec
### Opis
Robot zaprojektowany został z myślą o uczestnictwie w zawodach robotycznych w kategorii micro sumo (wymiary 5x5x5cm oraz waga do 100 gramów). 

Serce stanowi 8-bitowy układ Atmega. Robot wykorzystuje czujniki odległości w postaci [modułów Pololu](https://www.pololu.com/product/2578) oraz linii (2x ktir0711s). Całośc zasilana jest z litowo-polimerowego pakietu dwucelowego dającego namięcie nominalne 7,4V.

Płytka PCB została zaprojektowana z użyciem oprogramowania KiCAD oraz wykonana z użyciem techniki termotransferu. Obudowa została wydrukowana na drukarce 3D - tak samo, jak felgi kół. Opony zostały odlane z silikonu formiarskiego. Robot posiada również pług opadający zaraz po rozpoczęciu walki.
### Zdjęcia
![Zdjęcie 1](/Zdjecia/przod.jpg "Widok z przodu")
![Zdjęcie 2](/Zdjecia/tyl.jpg "Widok z tyłu")
### Uwagi
* Słabe silniki nie umieszczone współosiowo
* Mało czułe czujniki odległości